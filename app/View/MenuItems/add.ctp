<div class="menuItems form">
<?php echo $this->Form->create('MenuItem'); ?>
	<fieldset>
		<legend><?php echo __('Add Menu Item'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('order');
		echo $this->Form->input('controller');
		echo $this->Form->input('action');
		echo $this->Form->input('parameter');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Menu Items'), array('action' => 'index')); ?></li>
	</ul>
</div>
