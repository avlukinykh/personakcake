<div class="about">	
	<div class="header">
		<h1 class="about__header">
			Hello, I am
			<span class="about__name">Robb Armstrong</span>
			<br /> Designer and Front-end Developer
			<br />
			<hr class="about__line">
		</h1>
	</div>
	<p class="about__text">I have ten years experience as a web/interface designer. I have a love of clean, ele-<br />
		gant styling, and I have lots of experience in the production of CSS and (X)HTML for modern websites. I have a reasonable grasp of using JavaScript frameworks, specifi- <br />cally jQuery.
	</p>
</div>
<hr class="line">
<div class="contacts">
	<h1 class="contacts__header">
		Personal Info <br />
		<hr class="contacts__touch">
	</h1>
	<div class="contacts__info">
		<table class="contacts__label">
			<tr class="contacts__line">
				<td class="contacts__field">name</td>
				<td class="contacts__data">Robb Armstrong</td>
			</tr>
		</table>
		<table class="contacts__label">
			<tr class="contacts__line">
				<td class="contacts__field">data of birth</td>
				<td class="contacts__data">September 06, 1976</td>
			</tr>
		</table>
		<br />
		<table class="contacts__label">
			<tr class="contacts__line">
				<td class="contacts__field">e-mail</td>
				<td class="contacts__data">info@yourdomain.com</td>
			</tr>
		</table>
		<table class="contacts__label">
			<tr class="contacts__line">
				<td class="contacts__field">address</td>
				<td class="contacts__data">22 April, 98, Russian</td>
			</tr>
		</table>
		<br />
		<table class="contacts__label">
			<tr class="contacts__line">
				<td class="contacts__field">phone</td>
				<td class="contacts__data">012-345-678-9</td>
			</tr>
		</table>
		<table class="contacts__label">
		<tr class="contacts__line">
			<td class="contacts__field">website</td>
			<td class="contacts__data">www.themeforest.net</td>
		</tr>
		</table>
	</div>
</div>
<br />