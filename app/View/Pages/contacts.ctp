<!-- <div class="content"> -->
	<div class="header">
		<h1 class="header__title">CONTACT US</h1>
		<div class="header__next-page">
			<h2 class="header_navigating-pages">Go to next / previous page</h2>
			<div class="header__arrow">
				<i class="fa fa-arrow-circle-left"></i>
				<i class="fa fa-arrow-circle-right"></i>
			</div>
		</div>
	</div>
	<hr class = "line">
	<div class="map">
	<?php echo $this->Html->image('Map.png', array ('class' => 'map__img'));?>
	</div>
	<div class="text-info">
		<h1 class="text-info__title">Contact info</h1>
		<div class="text-info__text">
			<p>This is Photoshop's version  of Lorem Ipsum. 
				Proin gravida nibh vel velit auctor aliquet. 
				Aenean sollicitudin, lorem quis bibendum auctor, 
				nisi elit consequat ipsum, nec sagittis sem nibh id elit. 
				Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. 
				Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio.
			</p>
		</div>
		<div class="contact">
				<i class="fa fa-home"></i><span class="contact__street">lorem ipsum street</span>
				<i class="fa fa-phone"></i><span class="contact__number">+399 (500) 321 9548</span>
				<i class="fa fa-envelope"></i><span class="contact__email">info@domain.com</span>
		</div>
	</div>
	<br />
	<hr class = "line">
	<div class="title">
		<h2>Send us a message</h2>
	</div>
	<?php echo $this->element('sendMessage'); ?>
<!-- </div> -->
