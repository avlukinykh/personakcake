<div class="header">
	<h1 class="header__title">BLOG</h1>
	<div class="header__next-page">
		<h2 class="header_navigating-pages">Go to next / previous page</h2>
	<div class="header__arrow">
		<i class="fa fa-arrow-circle-left"></i>
		<i class="fa fa-arrow-circle-right"></i>
	</div>
</div>
</div>
<hr class="line">
<div class="picture">
	<?php echo $this->Html->image('img.png', array ('class' => 'picture__img'));?>
</div>
<div class="post">
	<h2 class="post__title">Standard Post with Image</h2>
		<div class="post__about-blog">
			<span class="post__signature"><i class="fa fa-calendar-o fa-lg"></i>&nbsp&nbsp&nbsp30 march</span>
			<span class="post__signature"><i class="fa fa-user fa-lg"></i>&nbsp&nbsp&nbspAdmin</span>
			<span class="post__signature"><i class="fa fa-comments fa-lg"></i>&nbsp&nbsp&nbsp2 comments</span>
		</div>
		<div class="post__text">
			<p class="postpost__paragraph">This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. 
			Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. 
			Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. 
			Nam nec tellus a odio tincidunt auctor a ornare odio.</p> 
			<p class="post__paragraph">Sed non  mauris vitae erat consequat auctor eu in elit. 
				Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. 
				Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. 
				Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. 
				Etiam pharetra, erat sed fermentum feugiat, velit mauris eges-<br />tas quam, ut aliquam massa nisl quis neque. 
				Suspendisse in orci enim.</p>
		</div>
	<hr class="line">
</div>
<div class="comments">
	<h2 class="comments__count">5 Comments</h2>
	<div class="comments__comment1">
		<div class="comments__border">
			<?php echo $this->Html->image('avatar.png', array ('class' => 'comments__avatar'));?>
		</div>
			
		<div class="comments__mess">
			<h4 class="comments__name">Collis Ta’eed<span class="comments__time">&nbsp&nbsp47 minutes ago</span></h4>
			<p class="comments__text-comment">Testing the comments</p>
			<input class="comments__reply" type="submit" value="reply"></p>
		</div>
	</div>
	<div class="comments__comment2">
		<div class="comments__border">
			<?php echo $this->Html->image('avatar.png', array ('class' => 'comments__avatar'));?>
		</div>
		<div class="comments__mess">
			<h4 class="comments__name">Jeffrey Way<span class="comments__time">&nbsp&nbsp47 minutes ago</span></h4>
			<p class="comments__text-comment">Works  good</p>
			<input class="comments__reply" type="submit" value="reply"></p>
		</div>
	</div>
	<div class="comments__comment3">
		<div class="comments__border">
			<?php echo $this->Html->image('avatar.png', array ('class' => 'comments__avatar'));?>
		</div>
		<div class="comments__mess">
			<h4 class="comments__name">John Smith<span class="comments__time">&nbsp&nbsp47 minutes ago</span></h4>
			<p class="comments__text-comment">lorem ipsum dolor sit amet</p>
			<input class="comments__reply" type="submit" value="reply"></p>
		</div>
	</div>
	<div class="comments__last">
		<div class="comments__border">
			<?php echo $this->Html->image('avatar.png', array ('class' => 'comments__avatar'));?>
		</div>
		<div class="comments__mess">
			<h4 class="comments__name">Collis Ta’eed<span class="comments__time">&nbsp&nbsp47 minutes ago</span></h4>
			<p class="comments__text-comment">dolor sit amet</p>
			<input class="comments__reply" type="submit" value="reply"></p>
		</div>
	</div>
	<hr class="line">
	<div class="title">
		<h2>Leave a comment</h2>
	</div>
	<?php echo $this->element('sendMessage'); ?>
</div>
