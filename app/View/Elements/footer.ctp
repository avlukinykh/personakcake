<div class="footer">
	<div class="footer__copyright">
		<pre><p class="footer__copyright-text">&copy; 2014 Robb Armstrong,  All Rights Reserved</p></pre>
	</div>
		<ul class="social">
			<li class="social__item"><a href="" class="social__icon"><i class="fa fa-facebook"></i></a>"></li>
			<li class="social__item"><a href="" class="social__icon"><i class="fa fa-twitter"></i></a>"></li>
			<li class="social__item"><a href="" class="social__icon"><i class="fa fa-dribbble"></i></a></li>
			<li class="social__item"><a href="" class="social__icon"><i class="fa fa-pinterest"></i></a></li>
		</ul>
</div>