<div class="sidebar">
	<div class="logo">
		<div class="logo__border-img">
			<?php echo $this->Html->image('photo.png', array ('class' => 'logo__img'));?>
		</div>
	</div>
	<nav class="menu">
		<ul class="menu__items">
			<li class="menu__item">
				<!-- <a href="" class="menu__link">Profile&nbsp&nbsp&nbsp<i class="fa fa-user fa-lg pull-right"></i></a> -->
				<?php echo $this->Html->link(
									'<br />Profile&nbsp&nbsp <i class="fa fa-user fa-lg pull-right"></i>', 
									'/', 
									array('escape' => false, 'class' => 'menu__link')
								);
								?>
			</li>
			<li class="menu__item">
								<?php echo $this->Html->link(
									'<br />Contacts&nbsp&nbsp <i class="fa fa-user fa-lg pull-right"></i>', 
									'/contacts', 
									array('escape' => false, 'class' => 'menu__link')
								);
								?>
				<!-- <a href="" class="menu__link">Work&nbsp&nbsp<i class="fa fa-briefcase fa-lg pull-right"></i></a -->>
			</li>
			<li class="menu__item">
				<?php echo $this->Html->link(
									'<br />About&nbsp&nbsp <i class="fa fa-user fa-lg pull-right"></i>', 
									'/about', 
									array('escape' => false, 'class' => 'menu__link')
								);
								?>
				<!-- <a href="" class="menu__link">Resume&nbsp&nbsp&nbsp<i class="fa fa-file-text fa-lg pull-right"></i></a> -->
			</li>
		</ul>
	</nav>
</div>