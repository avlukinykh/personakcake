<div class="send-messages">
				<?php
				echo $this->Form->create('Send-messages', array(
					'class' => 'send-messages__send'
					)
				);
				?>
		<div class="send-messages__first-colom">

				<?php
				echo $this->Form->input('Message.name', array(
					'class' => 'send-messages__data', 
					'placeholder' => 'name'
					)
				);
				echo $this->Form->input('Message.email', array(
					'class' => 'send-messages__data',
					'placeholder' => 'e-mail'
					)
				);
				echo $this->Form->input('Message.website', array(
					'class' => 'send-messages__data', 
					'placeholder' => 'website'));
			?> 
		</div>
		<div class="send-messages__second-colom">
			<?php
				echo $this->Form->input('Message.body', array(
					'class' => 'send-messages__message', 
					'placeholder' => 'message', 
					'type' => 'textarea'
					)
				);
				echo $this->Form->submit('Send Message', array(
					'class' => 'send-messages__button'
					)
				);
			?> 
		</div>
			<?php
				$this->Form->end();
			?>
</div>

