<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

        public function admin_index() {
        $this->User->recursive = 0;
        $this->set('users', $this->paginate());
    }

    public function admin_view($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->set('user', $this->User->read(null, $id));
    }

    public function admin_add() {
        if ($this->request->is('post')) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {

                    // $Email = new CakeEmail();
                    // $Email->from(array('me@example.com' => 'My Site'));
                    // $Email->to($this->request->data['massages']['name']);
                    // $Email->subject('About');
                    // $Email->send($this->request->data['messages']['body']);

                $this->Session->setFlash(__('The user has been saved'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(
                __('The user could not be saved. Please, try again.')
            );
        }
    }

    public function admin_edit($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The user has been saved'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(
                __('The user could not be saved. Please, try again.')
            );
        } else {
            $this->request->data = $this->User->read(null, $id);
            unset($this->request->data['User']['password']);
        }
    }

    public function delete($id = null) {
        // Prior to 2.5 use
        // $this->request->onlyAllow('post');

        $this->request->allowMethod('post');

        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->User->delete()) {
            $this->Session->setFlash(__('User deleted'));
            return $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('User was not deleted'));
        return $this->redirect(array('action' => 'index'));
    }

    public function beforeFilter() {
	    parent::beforeFilter();
	    // Allow users to register and logout.
        $this->Auth->allow('admin_add', 'logout', 'admin_login');
	}

	public function admin_login() {
	    if ($this->request->is('post')) {
            // pr($this->request->data);
            // exit;
	        if ($this->Auth->login()) {
	            return $this->redirect(array('controller'=>'messages', 'action'=>'index', 'admin'=>true));
	        }
	        $this->Session->setFlash(__('Invalid username or password, try again'));
	    }
	}

	public function admin_logout() {
	    // return $this->redirect($this->Auth->logout());
        if ($this->Auth->logout()) {
            $this->redirect('/');
        }
	}
}
