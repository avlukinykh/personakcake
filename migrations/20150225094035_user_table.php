<?php

use Phinx\Migration\AbstractMigration;

class UserTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->query("CREATE TABLE `users` (
            `id` INT(11) UNSIGNED AUTO_INCREMENT,
            `username` VARCHAR(50),
            `password` VARCHAR(255),
            `role` VARCHAR(20),
            `created` DATETIME DEFAULT NULL,
            `modified` DATETIME DEFAULT NULL,
            PRIMARY KEY (`id`)
            )
            ENGINE=InnoDB;"
        );
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->query("DROP  TABLE IF EXISTS `users`");
    }
}