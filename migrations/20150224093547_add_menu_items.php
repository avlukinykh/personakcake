<?php

use Phinx\Migration\AbstractMigration;

class AddMenuItems extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->query("CREATE TABLE `menu_items` (
            `id` INT(11) NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(250) NULL DEFAULT NULL,
            `order` INT(11) NULL DEFAULT NULL,
            `controller` VARCHAR(250) NULL DEFAULT NULL,
            `action` VARCHAR(250) NULL DEFAULT NULL,
            `parameter` VARCHAR(250) NULL DEFAULT NULL,
            PRIMARY KEY (`id`)
        )
        ENGINE=InnoDB;");
    }


    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->query("DROP  TABLE IF EXISTS `menu_items`");
    }
}