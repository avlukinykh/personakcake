<?php

use Phinx\Migration\AbstractMigration;

class AddMessages extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->query("CREATE TABLE `messages` (
            `id` INT(11) NOT NULL,
            `name` VARCHAR(250) NULL DEFAULT NULL,
            `email` VARCHAR(250) NULL DEFAULT NULL,
            `website` VARCHAR(250) NULL DEFAULT NULL,
            `body` TEXT NULL,
            `created` DATETIME NULL DEFAULT NULL,
            `updated` DATETIME NULL DEFAULT NULL,
            PRIMARY KEY (`id`)
        )
        ENGINE=InnoDB;"
        );
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->query("DROP  TABLE IF EXISTS `messages`");
    }
}